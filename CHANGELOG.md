
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.1.0] - 2022-11-14

#### Enhancement

- [#22890] Extend the MetadataDiscovery logic
- [#23537] Introduce the fieldId in the gCube Metadata Profile 
- Moved to maven-portal-bom 3.6.4
- [#24110] Added dependency required for building with JDK_11

## [v1.0.0] - 2020-09-30

#### First release

- [#19880] Create the library metadata-profile-discovery