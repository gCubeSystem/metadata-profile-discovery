/**
 *
 */
package org.gcube.common.metadataprofilediscovery;

import java.util.List;

import org.gcube.common.metadataprofilediscovery.bean.MetadataProfile;
import org.gcube.common.metadataprofilediscovery.jaxb.MetadataFormat;
import org.gcube.common.metadataprofilediscovery.jaxb.NamespaceCategory;


/**
 * The Interface MetadataProfileDiscovery.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Sep 30, 2020
 */
public interface MetadataProfileDiscovery {

	/**
	 * Gets the list of metadata types.
	 *
	 * @return the list of metadata types
	 * @throws Exception the exception
	 */
	List<MetadataProfile> getListOfMetadataProfiles() throws Exception;



	/**
	 * Gets the list of namespace categories.
	 *
	 * @return the list of namespace categories
	 * @throws Exception the exception
	 */
	List<NamespaceCategory> getListOfNamespaceCategories() throws Exception;


	/**
	 * Gets the metadata format for metadata type.
	 *
	 * @param type the type
	 * @return the metadata format for metadata type
	 * @throws Exception the exception
	 */
	MetadataFormat getMetadataFormatForMetadataProfile(MetadataProfile type) throws Exception;


	/**
	 * Reset metadata profile.
	 * Forces current list of Metadata Profile at null in order to read it from IS againg
	 */
	void resetMetadataProfile();

	/**
	 * Reset namespace categories.
	 * Forces current list of Namespace Categories Profile at null in order to read it from IS againg
	 */
	void resetNamespaceCategories();

	/**
	 * Return the XSD of the profile schema.
	 *
	 * @return the String representation of the XSD containing the schema for a profile
	 */
	String getProfileSchema();
	
	/**
	 * Validate the xml provided as argument.
	 *
	 * @param xmlProfile the string representation of the XML to validate again the schema
	 * @throws Exception the exception
	 */
	void validateProfile(String xmlProfile) throws Exception;

	
}
