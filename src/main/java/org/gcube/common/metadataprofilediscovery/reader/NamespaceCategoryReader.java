/**
 *
 */
package org.gcube.common.metadataprofilediscovery.reader;

import static org.gcube.resources.discovery.icclient.ICFactory.client;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.gcube.common.metadataprofilediscovery.jaxb.NamespaceCategories;
import org.gcube.common.resources.gcore.utils.XPathHelper;
import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.Query;
import org.gcube.resources.discovery.client.queries.impl.QueryBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


/**
 * The Class NamespaceCategoryReader.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Apr 26, 2017
 */
public class NamespaceCategoryReader {

	private static final String PATH_RESOURCE_PROFILE_BODY_NAMESPACES = "/Resource/Profile/Body/namespaces";

	private static Logger logger = LoggerFactory.getLogger(NamespaceCategoryReader.class);

	private static final String GENERIC_RESOURCE_NAMESPACES_NAME = "Namespaces Catalogue Categories";
	private static final String GENERIC_RESOURCE_NAMESPACES_SECONDARY_TYPE = "DataCatalogueNamespace";

	private ScopeBean scope;

	private NamespaceCategories namespaces;

	/**
	 * Instantiates a new medata format reader.
	 *
	 * @param scope the scope
	 * @throws Exception the exception
	 */
	public NamespaceCategoryReader(ScopeBean scope) throws Exception {
		this.scope = scope;
		readNamespaceCategory();
	}

	/**
	 * Gets the metadata format by id.
	 *
	 * @return the metadata format by id
	 * @throws Exception the exception
	 */
	private void readNamespaceCategory() throws Exception {
		logger.trace("Getting Namespace Category");

		if(this.scope==null)
			throw new Exception("Scope is null");

		String scopeString = this.scope.toString();
		logger.trace("read scope: "+scopeString);

		namespaces = new NamespaceCategories();

		try {

			logger.info("Using scope from ScopeProvider: "+scopeString);

			String queryString = QueryForResourceUtil.getGcubeGenericQueryStringForSecondaryTypeAndName(GENERIC_RESOURCE_NAMESPACES_NAME, GENERIC_RESOURCE_NAMESPACES_SECONDARY_TYPE);
			logger.trace("queryString: " +queryString);
			Query q = new QueryBox(queryString);

			DiscoveryClient<String> client = client();
			List<String> appProfile = client.submit(q);

			if (appProfile == null || appProfile.size() == 0)
				throw new MetadataProfileNotFoundException("Your resource is not registered in the infrastructure, the scope is "+scopeString);
			else {
				String theResource = null;
				try{
					theResource = appProfile.get(0);
					logger.debug("Resource "+GENERIC_RESOURCE_NAMESPACES_NAME+" found");
					logger.trace("Resource "+GENERIC_RESOURCE_NAMESPACES_NAME+" is: "+theResource);
					DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document xmlDocument  = docBuilder.parse(new InputSource(new StringReader(theResource)));
					readNamespaceCategoryFromResource(xmlDocument);
				}catch(Exception e){
					logger.error("Error while parsing Resource "+theResource+" from the infrastructure, the scope is "+scopeString,e);
				}
			}

		} catch (Exception e) {
			logger.error("Error while trying to fetch resource with name " + GENERIC_RESOURCE_NAMESPACES_NAME + " and secondary type " + GENERIC_RESOURCE_NAMESPACES_SECONDARY_TYPE + 
					" from the infrastructure, the scope is "+scopeString, e);
			throw new Exception("Error while trying to fetch resource with name " + GENERIC_RESOURCE_NAMESPACES_NAME + " and secondary type " + GENERIC_RESOURCE_NAMESPACES_SECONDARY_TYPE + 
					" from the infrastructure, the scope is "+scopeString, e);
		} finally{
		}
	}

	/**
	 * Read namespace category from resource.
	 *
	 * @param helper the helper
	 * @throws Exception the exception
	 */
	private void readNamespaceCategoryFromResource(Document xmlDocument) throws Exception{

		try {
			
			XPath xPath = XPathFactory.newInstance().newXPath();
			String expression = PATH_RESOURCE_PROFILE_BODY_NAMESPACES;
			NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
			
			String namespaceSources = getNodeString(nodeList.item(0));

			JAXBContext jaxbContext = JAXBContext.newInstance(NamespaceCategories.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			if(namespaceSources==null || namespaceSources.length()==0){
				throw new Exception("Resource does not contain <namespaces> <namespace>...</namespace> </namespaces> in the body");
			}
			
			logger.debug("Unmarshalling document path "+PATH_RESOURCE_PROFILE_BODY_NAMESPACES +" to "+NamespaceCategories.class.getSimpleName());
			InputStream stream = new ByteArrayInputStream(namespaceSources.getBytes());
			namespaces = (NamespaceCategories) jaxbUnmarshaller.unmarshal(stream);

		}catch(Exception e){
			String error = "An error occurred in readNamespaceCategoryFromResource " + e.getMessage();
			logger.error("An error occurred in readNamespaceCategoryFromResource ", e);
			throw new Exception(error);
		}
	}
	
	private String getNodeString(Node node) {
	    try {
	        StringWriter writer = new StringWriter();
	        Transformer transformer = TransformerFactory.newInstance().newTransformer();
	        transformer.transform(new DOMSource(node), new StreamResult(writer));
	        String output = writer.toString();
	        return output;
	    } catch (TransformerException e) {
	        e.printStackTrace();
	    }
	    return node.getTextContent();
	}

	/**
	 * Read namespace category from resource.
	 *
	 * @param helper the helper
	 * @throws Exception the exception
	 */
	private void readNamespaceCategoryFromResource(XPathHelper helper) throws Exception{

		try {
			
			if(helper==null) {
				logger.warn("**************************\n\nXpathHelper is null!!!!\n\n");
			}
				

			List<String> namespaceSources = helper.evaluate(PATH_RESOURCE_PROFILE_BODY_NAMESPACES);

			JAXBContext jaxbContext = JAXBContext.newInstance(NamespaceCategories.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			if(namespaceSources==null || namespaceSources.size()==0){
				throw new Exception("Resource does not contain <namespaces> <namespace>...</namespace> </namespaces> in the body");
			}

			InputStream stream = new ByteArrayInputStream(namespaceSources.get(0).getBytes());
			namespaces = (NamespaceCategories) jaxbUnmarshaller.unmarshal(stream);

		}catch(Exception e){
			String error = "An error occurred in readNamespaceCategoryFromResource " + e.getMessage();
			logger.error("An error occurred in readNamespaceCategoryFromResource ", e);
			throw new Exception(error);
		}
	}

	/**
	 * Gets the namespaces.
	 *
	 * @return the namespaces
	 */
	public NamespaceCategories getNamespaces() {

		return namespaces;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("NamespaceCategoryReader [scope=");
		builder.append(scope);
		builder.append(", namespaces=");
		builder.append(namespaces);
		builder.append("]");
		return builder.toString();
	}

}
