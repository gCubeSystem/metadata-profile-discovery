package org.gcube.common.metadataprofilediscovery.reader;

@SuppressWarnings("serial")
public class PropertyFileNotFoundException extends Exception {
	 public PropertyFileNotFoundException(String message) {
	    super(message);
	  }
}