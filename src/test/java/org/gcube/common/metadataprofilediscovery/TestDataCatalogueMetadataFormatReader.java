/**
 *
 */
package org.gcube.common.metadataprofilediscovery;

import java.io.InputStream;
import java.util.List;

import org.gcube.common.metadataprofilediscovery.bean.MetadataProfile;
import org.gcube.common.metadataprofilediscovery.jaxb.MetadataFormat;
import org.gcube.common.metadataprofilediscovery.jaxb.NamespaceCategory;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class TestDataCatalogueMetadataFormatReader.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jun 8, 2016
 */
public class TestDataCatalogueMetadataFormatReader {

	private static final Logger logger = LoggerFactory.getLogger(TestDataCatalogueMetadataFormatReader.class);

	/**
	 * Test.
	 * @throws Exception 
	 */
	//@Test
	public void test() throws Exception {

			
//			String scopeString = "/gcube/devsec/devVRE";
//			String grMetadataProfileSecondaryType = "DataCatalogueMetadata";
		
		String scopeString = "/gcube/devsec/devVRE";
		String grMetadataProfileSecondaryType = "GeoNaMetadata";
		
		ScopeProvider.instance.set(scopeString);
		MetadataProfileReader reader = new MetadataProfileReader(grMetadataProfileSecondaryType);
		int i = 0;

		List<NamespaceCategory> categs = reader.getListOfNamespaceCategories();
		for (NamespaceCategory namespaceCategory : categs) {
			logger.info("\n\n "+ ++i +".) Category: "+namespaceCategory);
		}

		i = 0;
		for (MetadataProfile mt : reader.getListOfMetadataProfiles()) {

			if(mt==null)
				continue;

			MetadataFormat metadataFormat = reader.getMetadataFormatForMetadataProfile(mt);
			logger.info("\n\n "+ ++i +".) Metadata source: "+metadataFormat.getMetadataSource().substring(0, 100));
		}
	
	}
	
	
	public static String PROFILE_EXAMPLE_FILENAME = "EmptyProfileExample.xml";
	
//	@Test
	public void validateAgainstProfileSchema() throws Exception {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(PROFILE_EXAMPLE_FILENAME);
		MetadataProfileReader.validateProfile(inputStream);
	}
}


