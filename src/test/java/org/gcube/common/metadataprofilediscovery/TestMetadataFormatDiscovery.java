/**
 *
 */
package org.gcube.common.metadataprofilediscovery;

import org.gcube.common.metadataprofilediscovery.bean.MetadataProfile;
import org.gcube.common.metadataprofilediscovery.jaxb.MetadataField;
import org.gcube.common.metadataprofilediscovery.jaxb.MetadataFormat;
import org.gcube.common.metadataprofilediscovery.reader.MetadataFormatDiscovery;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.scope.impl.ScopeBean;


/**
 * The Class TestMetadataFormatDiscovery.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jun 8, 2016
 */
public class TestMetadataFormatDiscovery {

	/**
	 * Test.
	 */
	//@Test
	public void test() {

		String scopeString = "/gcube/devsec/devVRE";
		String grMetadataProfileSecondaryType = "DataCatalogueMetadata";
		grMetadataProfileSecondaryType = "GeoNaMetadata";

		final ScopeBean scope  = new ScopeBean(scopeString);
		MetadataFormatDiscovery reader;
		try {
			ScopeProvider.instance.set(scopeString);
			reader = new MetadataFormatDiscovery(scope, grMetadataProfileSecondaryType, null);
			//System.out.println(reader.getMetadataProfiles());

			for (MetadataProfile metaProfile : reader.getMetadataProfiles()) {
				System.out.println("\n\n###" + metaProfile.getId() +", name: "+metaProfile.getName() +", type: "+metaProfile.getMetadataType());
				
				MetadataProfileReader readerProfile = new MetadataProfileReader(grMetadataProfileSecondaryType, metaProfile.getName());
				MetadataFormat format = readerProfile.getMetadataFormatForMetadataProfile(metaProfile);
				int i = 0;
				try {
				for (MetadataField field : format.getMetadataFields()) {
					System.out.println("\t"+ ++i +") "+field);
				}
				}catch (Exception e) {
					System.err.println("\t## Error occurred reading: " + metaProfile.getId() +", name: "+metaProfile.getName() +", type: "+metaProfile.getMetadataType());
					e.printStackTrace();
				}
			}
			
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
