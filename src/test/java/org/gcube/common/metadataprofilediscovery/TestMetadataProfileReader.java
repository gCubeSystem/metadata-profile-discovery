/**
 *
 */
package org.gcube.common.metadataprofilediscovery;

import org.gcube.common.metadataprofilediscovery.bean.MetadataProfile;
import org.gcube.common.metadataprofilediscovery.jaxb.NamespaceCategory;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.scope.impl.ScopeBean;
import org.junit.Test;

/**
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jun 8, 2016
 */
public class TestMetadataProfileReader {

	//@Test
	public void test() {

		String scopeString = "/gcube/devsec/devVRE";
		final ScopeBean scope = new ScopeBean(scopeString);
		
		String[] genericResourceNames = new String[] {"Informazioni_di_progetto","Relazione_di_Scavo"};
		
		for (int i = 0; i < 2; i++) {
			MetadataProfileReader reader;
			try {
				ScopeProvider.instance.set(scope.toString());
				reader = new MetadataProfileReader("GeoNaMetadata",genericResourceNames[i]);

				int j = 0;
				for (MetadataProfile metadataProfile : reader.getListOfMetadataProfiles()) {
					System.out.println(j++ + ")" + metadataProfile);
				}

				j = 0;
				for (NamespaceCategory namespaceCategory : reader.getListOfNamespaceCategories()) {
					System.out.println(j++ + ")" + namespaceCategory);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
	}

}
