/**
 *
 */
package org.gcube.common.metadataprofilediscovery;

import java.io.InputStream;

import org.gcube.common.metadataprofilediscovery.jaxb.MetadataField;
import org.gcube.common.metadataprofilediscovery.jaxb.MetadataFormat;
import org.gcube.common.metadataprofilediscovery.reader.MetadataFormatReader;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.scope.impl.ScopeBean;

/**
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jun 8, 2016
 */
public class TestMetadataFormatReader {

//	@Test
	public void test() {

		String scopeString = "/gcube/devsec/devVRE";
		final ScopeBean scope = new ScopeBean(scopeString);
		MetadataFormatReader reader;
		try {
			ScopeProvider.instance.set(scopeString);
			reader = new MetadataFormatReader(scope, "0d29d7a9-d779-478c-a13d-d70708dc66c4");
			System.out.println(reader.getMetadataFormat());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// @Test
	public void testReadInputStream() {
		String fileNameMeatadataProfile = "HarvestedObject.xml";
		try {
			InputStream in = ClassLoader.getSystemResourceAsStream(fileNameMeatadataProfile);
			// InputStream in =
			// TestMetadataFormatReader.class.getResourceAsStream(fileNameMeatadataProfile);
			MetadataFormat mf = MetadataProfileReader.toMetadataFormat(in);
			System.out.println("Source: " + mf.getMetadataSource());

			for (MetadataField field : mf.getMetadataFields()) {
				System.out.println(field);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
